<?php 

include_once("Micromodel/UserDAOImpl.php");

class Controller
{
    public $UserDAOImpl;
    public function __construct()
    {
        $this->UserDAOImpl = new UserDAOImpl();
    }
    public function invoke()
    {
        if (isset($_GET['forgotpassword'])) {
                        // $login=$_GET['login'];
            $obj = json_decode($_GET["forgotpassword"], false);
            $Email = $obj->Email;
            $bool = $this->UserDAOImpl->forgotPassword($Email);


            echo $bool;
            //// by suggestion
        } elseif (isset($_GET['follow'])){
            $id = $_GET["id"];
            $bool = $this->UserDAOImpl->follow($id);
            echo $bool;
            // by profile
        } elseif (isset($_GET['followidbyprofile'])){
            $id = $_GET["followidbyprofile"];
            $bool = $this->UserDAOImpl->follow($id);
            echo $bool;
        
        } elseif (isset($_GET['editprofile'])) {
            $obj = json_decode($_GET['editprofile'],false);
            $editFirstName = $obj->registerFirstName;
            $editLastName = $obj->registerLastName;
            $editPassword = $obj->registerPassword;
            $checker = $this->UserDAOImpl->getEditUser($editFirstName,$editLastName,$editPassword );
            echo $checker;
        } elseif (isset($_GET['picture'])) {
            $string = $this->UserDAOImpl->getPictures($_GET['picture']);
            echo $string;
        } elseif (isset($_GET['register'])) {
                        // $login=$_GET['login'];
            $obj = json_decode($_GET["register"], false);
            $registerFirstName = $obj->registerFirstName;
            $registerLastName = $obj->registerLastName;
            $registerPassword = $obj->registerPassword;
            $registerEmail = $obj->registerEmail;


            $checker = $this->UserDAOImpl->getUserEmail($registerEmail);
            if ($checker == false) {
                $bool = $this->UserDAOImpl->getRegister($registerFirstName, $registerLastName, $registerPassword, $registerEmail);

                echo $bool;
            } else {
                echo 'Email Already registered. Please Login';

            }
            
        } elseif (isset($_GET['password'])) {
            // $login=$_GET['login'];
            $obj = json_decode($_GET["password"], false);
            $UserID = $obj->UserID;
            $newpass = $obj->newpass;
            $oldpass = $obj->oldpass;
            if ($oldpass !== $newpass) {
                echo 'Password does not match';
            } else {
                $bool = $this->UserDAOImpl->resetPassword($newpass, $UserID);


                echo $bool;
            }
        } elseif (isset($_GET['limit'])) {
            $limit = $_GET['limit'];
            $pageCount=$_GET["pageCount"];
            $linkCount=$_GET["linkCount"];
            $bool = json_encode($this->UserDAOImpl->paginationTweets($limit,$pageCount,$linkCount));
            echo $bool;
        } elseif (isset($_GET['editweets'])) {
            // $login=$_GET['login'];
            $obj = json_decode($_GET["editweets"], false);
            $TweetID = $obj->TweetId;
            $TweetContent = $obj->TweetContent;
            $TweetDate = $obj->Tweetdate;
            $bool = $this->UserDAOImpl->editTweet($TweetID, $TweetContent,$TweetDate);
            echo $bool;
        } elseif (isset($_POST['query'])) {
            // $login=$_GET['login'];
            $obj = $_POST['query'];
            $bool = $this->UserDAOImpl->getQuery($obj);
            echo $bool;
            return $bool;
        } elseif (isset($_POST['image'])) {
            // $login=$_GET['login'];
            $image = json_decode($_POST["image"]);

            $bool = $this->UserDAOImpl->getImage($image->Image);
            echo $bool;

        } elseif (isset($_GET['login'])) {
                   // $login=$_GET['login'];
            $obj = json_decode($_GET["login"], false);
            $loginEmail = $obj->loginName;
            $password = $obj->password;
            $checker = $this->UserDAOImpl->searchtemp_user($loginEmail);
            if ($checker == false) {
                $bool = $this->UserDAOImpl->getUser($loginEmail, $password);
                if ($bool === false) {
                    echo 'Unsuccessful login';
                } else
                    echo $bool;

            } else {
                echo $checker[0];
            }

           
        } else {

            include 'Microview/login.php';

        }

    }

    public function loginView($modelusername)
    {
        $loginUser = $modelusername;
        include 'Microview/profile.php';
    }
    public function Register()
    {

    }


}


?>