<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Microblog Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/MicroAjax/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script src="/MicroAjax/main.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/Template.css" />
</head>
<script>
$(document).ready(function() { 
	    $('#btnSubmit').on('click', function() { 
            $('#div1-wrapper').load(' #div1'); 
}); 
});     

</script>
<body>


<div id="mySidenav" class="sidenav">
<?php 
if (!isset($_COOKIE['user'])) {
    echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
    echo '<a href="#">About</a>';
    echo '<a href="login.php">Login</a>';
    echo '<a href="register.php">Register</a>';
    echo '<a href="#">Contact</a>';
} else {
    echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
    echo '<a href="profile.php?name=' . $_COOKIE['user'] . '">Profile</a>';
    echo '<a href="profileedit.php">Settings</a>';
    echo '<a href="logout.php">Logout</a>';
}
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

?>
</div>
<div class="w3-blue-background w3-right w3-container"></div>

<div class="w3-black w3-bar w3-large">
<?php 
if (isset($_COOKIE['user']))
    echo ' <a href="loggedinHome2.php?name=' . $_COOKIE['user'] . '"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
else
    echo ' <a href="login.php?"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
?>
  <a href="#" class="w3-bar-item w3-button w3-mobile w3-margin-left">About</a>
  <input type="text" class="w3-bar-item w3-input w3-white w3-mobile" placeholder="Search.." id="Searchbar" style="margin-left: 250px">
  <div id="livesearch"></div> 
     <button class="w3-bar-item w3-button w3-black w3-mobile"id = "Go">Go</button>
  <span style="font-size:30px;cursor:pointer" class ="w3-right w3-margin-right" onclick="openNav()">&#9776; Menu</span>
</div>
        <div id="main">

<div class="row" >

  <div class="column side" style="background-color:#aaa;">
  
  <center><div id="testimage" ></div></center>

  <?php 
 
  /////////////////////////////////////////////////////Start of first column/////////////////////////////////////////////////////

    chdir('../Micromodel');

    include 'db.php';

    $identification = $_GET['name'];
    echo '<script type="text/javascript">';
    echo 'pictures(' . $identification . ');';
    echo '</script>';
    $db = "microblog";
    $conn = dbConnection($db);
    $stmt = $conn->prepare("SELECT * From `user` where UserID=?");
    $stmt->bind_param("s", $identification);
    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {



                echo 'Hello user: ' . $row['Lastname'] . ' ';
                echo $row['Firstname'] . '<br>';
                echo 'Your Email Address:' . $row['Email'] . '<br>';
            }

        }
    }

    $stmt2 = $conn->prepare("SELECT * FROM `following`as following LEFT Join `user` as users on users.UserID = following.UserID where FollowersID=?");

    $stmt2->bind_param("s", $identification);
    echo 'here are the people  you are following<br>';
    if ($stmt2->execute()) {
        $result = $stmt2->get_result();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $filename = ('../microview/uploads/' . $row['Email'] . '.jpg');
                if (file_exists($filename)) {
                    $name = $row['Email'];
                } else
                    $name = "default";
                echo '<img src="../microview/uploads/' . $name . '.jpg" style = "height: 100px ;width: 100px ;float:left ;"><br><br>';
                echo '<a href="profile.php?name=' . $row['UserID'] . '">';
                echo $row['Firstname'] . $row['Lastname'] . '<br>';
                echo '</a>';
                echo $row['Email'] . '<br><hr>';
            }
        }
    }
    if (isset($_COOKIE['user'])) {
        if (($_COOKIE['user'] !== $_GET['name'])) {
            $suggestArray = fetchSuggestedArray2($conn, $identification);
            if (!empty($suggestArray)) {
                $i = 0;
                $flag = true;
                foreach ($suggestArray as $key) {
                    //$suggestArray[] = array($suggestFname = $row['Firstname'], $suggestLname = $row['Lastname'],$suggestID = $row['ID']);

                    if ($_GET['name'] === $key[2]) {
                        echo '<input type="button"  name = "followme" class="w3-btn w3-orange" value = "UnfollowMe" id="followMe">';
                        $flag = false;
                    }
                    $i++;
                }
                if ($flag === true) {
                    echo '<input type="button"  name = "followme" class="w3-btn w3-orange" value = "followMe" id="followMe">';
                }
            }
        //  
        }
    }
    /////////////////////////////////////////////////////End of first ////////////////////////////////////////

    ?>
    <input type="hidden" id="UserID" id='UserID' value="<?php echo $_GET['name'] ?>">
  
  </div>
  <div class="column middle" style="background-color:#bbb;">
  <form method="post">
        Enter Tweet here : <input type="text" name="hashtags" id="hashtags">
        <Center><input type="submit" name = "button[]" class="w3-btn w3-orange" value="submit"  id="btnSubmit"></Center>
    </form>
    <hr>

    <?php 
    /////////////////////////////////////////////////////Start of Second column////////////////////////////////////////
    if (!isset($_COOKIE['user'])) {
        echo "Cookie named  is not set!";
    } else {
        echo "Hello : " . $_COOKIE['user'];
    }
    chdir('../Micromodel');
    $identification = $_GET['name'];
    $db = "microblog";
    $conn = dbConnection($db);



    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $tweetsArray = fetchArrayTweets($conn, $identification);
        $suggestArray = fetchSuggestedArray2($conn, $identification);

        $i = 0;
        if (isset($_POST['button'])) {
            foreach ($_POST['button'] as $key) {
                switch ($key) {
                    case 'delete':
                        $var = 'delete';
                        break;
                    case 'retweet':
                        $var = 'retweet';
                        break;
                    case 'heart':
                        $var = 'heart';
                        break;
                    case 'submit':
                        $var = 'submit';
                        break;
                    case 'follow':
                        $var = 'follow';
                        break;
                    case 'edit':
                        $var = 'edit';
                        break;
                    case 'unheart':
                        $var = 'unheart';
                        break;
                    default:
                        break;
                }
            }
        }
        if (!empty($tweetsArray)) {
            foreach ($tweetsArray as $key) {
                if (key($_POST['button']) == $i && (strcmp($var, "delete") === 0 || strcmp($var, "retweet") === 0 || strcmp($var, "heart") === 0 || strcmp($var, "edit") === 0 || strcmp($var, "unheart") === 0)) {
                    $content = $key[0];
                    $firstname = $key[1];
                    $lastname = $key[2];
                    $date = $key[3];
                    $id = $key[4];
                    $userid = $key[5];
                    if (sizeof($key) == 7) {
                        $retweetid = $key[6];
                    }
                }
                $i++;
            }
        }

        //////////////Follow looop
        if (!empty($suggestArray)) {
            $i = 0;
            foreach ($suggestArray as $key) {
                //$suggestArray[] = array($suggestFname = $row['Firstname'], $suggestLname = $row['Lastname'],$suggestID = $row['ID']);

                if (key($_POST['button']) == $i && strcmp($var, "follow") === 0) {
                    $SuggestID2 = $key[2];
                }
                $i++;
            }
        }
        $mysqltimeCurrent = date("Y-m-d H:i:s");

        ////////////End of follow loop
        if (strcmp($var, "submit") === 0) {
            $textarea = $_POST["hashtags"];
            $textarea = test_input($textarea);
            $stmt = $conn->prepare("INSERT INTO `tweets`( `UserID`, `Content`, `DateCreated`) VALUES (?,?,?)");
            $stmt->bind_param("sss", $identification, $textarea, $mysqltimeCurrent);
            $stmt->execute();
        } elseif (strcmp($var, "edit") === 0) {
            if ($userid != $_COOKIE["user"]) {
                echo '<script type="text/javascript">';
                echo 'editTweet(' . $id . ');';
                echo '</script>';
            } else {
                echo "<script>alert('you are not allowed to edit a retweet')</script>";
            }
        } elseif (strcmp($var, "retweet") === 0) {
            $stmt = $conn->prepare("INSERT INTO `retweet` (`TweetsID`,`UserID`,`DateCreated`) VALUES(?,?,?)");
            $identification = $_COOKIE["user"];
            $stmt->bind_param("sss", $id, $identification, $mysqltimeCurrent);
            $stmt->execute();
        } elseif (strcmp($var, "heart") === 0) {

            $stmt = $conn->prepare("INSERT INTO `hearts` (`TweetsID`,`UserID`,`DateHearted`) VALUES(?,?,?)");
            $stmt->bind_param("sss", $id, $identification, $mysqltimeCurrent);
            $stmt->execute();
        } elseif (strcmp($var, "unheart") === 0) {

            $stmt = $conn->prepare("DELETE FROM `hearts` where UserID = ? AND TweetsID = ?");
            $stmt->bind_param("ss", $identification, $id);
            $stmt->execute();

        } elseif (strcmp($var, "follow") === 0) {

            $stmt = $conn->prepare("INSERT INTO `following` (`UserID`,`FollowersID`,`DateFollowed`) VALUES(?,?,?) ");
            $stmt->bind_param("sss", $SuggestID2, $identification, $mysqltimeCurrent);
            $stmt->execute();

        } else {
            $stmt = $conn->prepare("DELETE FROM `retweet` where UserID = ? AND TweetsID = ?");
            $stmt->bind_param("ss", $identification, $id);
            $stmt->execute();

            $stmt = $conn->prepare("DELETE FROM `hearts` where UserID = ? AND TweetsID = ?");
            $stmt->bind_param("ss", $identification, $id);
            $stmt->execute();


            $stmt = $conn->prepare("DELETE FROM `tweets` where UserID = ? AND TweetsID = ?");
            $stmt->bind_param("ss", $identification, $id);
            $stmt->execute();



        }

    }
    tweet($conn, $identification);
    //$stmt2 = $conn->prepare("SELECT c2.Lastname, c2.Firstname , c1.UserID FROM `following` as c1 JOIN `user` as c2 on c1.FollowersID = c2.UserID WHERE c1.UserID = ? ");

 /////////////////////////////////////////////////////End of Second column/////////////////////////////////////////////////////

    ?>
    </div>
</div>
  <div>
  
  </div>
  


</div>
<?php 

?>

  <div class="column side" style="background-color:#ccc;"><?php 

/////////////////////////////////////////////////////Start of Third Column/////////////////////////////////////////////////////
                                                            if (isset($_COOKIE['user']))
                                                                if ($_COOKIE['user'] === $_GET['name']) {
                                                                chdir('../Micromodel');
                                                                $identification = $_GET['name'];
                                                                $db = "microblog";
                                                                $conn = dbConnection($db);
                                                                fetchsuggested($conn, $identification);
                                                            } else
                                                                echo 'Welcome!';
                                                            else {
                                                                echo 'Please login to see all the necessary details!';
                                                                echo '</div>';
                                                            }
////////////////////////////////////////////////////////End of Third Column/////////////////////////////////////////////////////

                                                            ?>
  
  </div>


</body><script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
</html>










<!--PHP Functions need to fix this :D -->






<?php 
function fetchsuggested($conn, $identification)
{
    $suggestfollow = fetchArrayFollow($conn, $identification);

        //Query all then one by one
    $stmt = $conn->prepare("SELECT * FROM `user` Where Not UserID = ?");
    $stmt->bind_param("s", $identification);
    echo '<div id ="div1-wrapper">';
    echo '<div id="div1"> ';
    if ($stmt->execute()) {
        $result = $stmt->get_result();
        echo 'here are the recommended people to follow <br>';
        $suggested = 0;
        $flag = false;
        if (!empty($suggestfollow)) {
            $counter = count($suggestfollow);
        }
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $flag = false;
                if (empty($suggestfollow)) {
                    $filename = ('../microview/uploads/' . $row['Email'] . '.jpg');
                    if (file_exists($filename)) {
                        $name = $row['Email'];
                    } else
                        $name = "default";
                    echo '<img src="../microview/uploads/' . $name . '.jpg" style = "height: 100px ;width: 100px ;float:left ;"><br>';

                    echo $row['Firstname'];
                    echo $row['Lastname'] . '<br>';
                    echo '<input type="submit" class="w3-btn w3-orange" name = "button[' . $suggested . ']" value = "follow"><br><hr>';
                    $suggested++;

                    $suggestArray[] = array($suggestFname = $row['Firstname'], $suggestLname = $row['Lastname'], $suggestID = $row['UserID']);
                } elseif ($flag == false) {
                    foreach ($suggestfollow as $key) {

                        if ($row["UserID"] == $key[0] && $key[1] == $identification) {
                            $flag = true;
                        }
                    }
                    if ($flag == false) {



                        $filename = ('../microview/uploads/' . $row['Email'] . '.jpg');
                        if (file_exists($filename)) {
                            $name = $row['Email'];
                        } else
                            $name = "default";
                        echo '<img src="../microview/uploads/' . $name . '.jpg" style = "height: 100px ;width: 100px ;float:left ;"><br>';
    
                        echo $row['Firstname'];
                        echo $row['Lastname'] . '<br>';
                        echo '<input type="submit" class="w3-btn w3-orange" name = "button[' . $suggested . ']" value = "follow"><br><hr>';
                        $suggested++;
    
                        $suggestArray[] = array($suggestFname = $row['Firstname'], $suggestLname = $row['Lastname'], $suggestID = $row['UserID']);
                    }
                }
               

            }
        }
    }
    echo '</div>';
    echo '</div>';
}
function fetchSuggestedArray2($conn, $identification)
{
    $suggestfollow = fetchArrayFollow($conn, $identification);

    //Query all then one by one
    $stmt = $conn->prepare("SELECT * FROM `user` Where Not UserID = ?");
    $stmt->bind_param("s", $identification);
    if ($stmt->execute()) {
        $result = $stmt->get_result();
        $suggested = 0;
        $flag = false;
        if (!empty($suggestfollow)) {
            $counter = count($suggestfollow);
        }
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $flag = false;
                if (empty($suggestfollow)) {
                    $suggested++;

                    $suggestArray[] = array($suggestFname = $row['Firstname'], $suggestLname = $row['Lastname'], $suggestID = $row['UserID']);
                } elseif ($flag == false) {
                    foreach ($suggestfollow as $key) {

                        if ($row["UserID"] == $key[0] && $key[1] == $identification) {
                            $flag = true;
                        }
                    }
                }
                if ($flag == false) {

                    $suggested++;

                    $suggestArray[] = array($suggestFname = $row['Firstname'], $suggestLname = $row['Lastname'], $suggestID = $row['UserID']);
                }

            }
            return $suggestArray;
        }
    }


}

function fetchArrayFollow($conn, $identification)
{
        //get the list of followed people by this user
    $stmt = $conn->prepare("SELECT * FROM  `following` as follow where follow.FollowersID = ?");
    $stmt->bind_param("s", $identification);
    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $suggestArray[] = array($suggestLname = $row['UserID'], $suggestID = $row['FollowersID']);
            }
            return $suggestArray;
        }
    }

}
function fetchArrayTweets($conn, $identification)
{
    $stmt = $conn->prepare("SELECT c1.TweetsID as tweetsID, c1.UserID as ID,c1.Content as Tweets ,c2.Firstname ,retweet.UserID as RetweetUserID,c2.Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate, retweet.RetweetID as retweetkey From `tweets` as c1 Left Join `retweet` as retweet on retweet.TweetsID = c1.TweetsID Left JOIN `user` as c2 on c2.UserID = retweet.UserID WHERE retweet.UserID = ? OR c1.UserID = ? ");

//        $stmt = $conn->prepare("SELECT c1.TweetsID as tweetsID, c1.UserID as ID,c1.Content as Tweets ,c2.Firstname ,c2.Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate  From `tweets` as c1 LEFT JOIN `user` as c2 on c1.UserID = c2.UserID RIGHT Join `retweet` as retweet on retweet.TweetsID = tweet.TweetsID WHERE retweet.UserID = 28 AND where c2.UserID = ?");
    $stmt->bind_param("ss", $identification, $identification);

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows == 0) {
            echo '<br> You have not twitted yet <br>';
        }
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $my_array[] = array($tweets = $row["Tweets"], $Firstname = $row["Firstname"], $Lastname = $row["Lastname"], $DateCreated = $row["Date"], $tweetsId = $row["tweetsID"], $userid = $row["RetweetUserID"], );

            }
            return $my_array;
        }
    }
}
function checkHearts($conn, $identification)
{
    $my_array = null;
    $stmt = $conn->prepare("SELECT * From `tweets` as c1 Left Join `hearts` as heart on heart.TweetsID = c1.TweetsID Left JOIN `user` as c2 on c2.UserID = heart.UserID WHERE heart.UserID = ? ");
    $stmt->bind_param("s", $identification);
    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {
                $my_array[] = array($tweets = $row["TweetsID"]);
            }
            return $my_array;
        }
    }
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function tweet($conn, $identification)
{           //SELECT * From `tweets` as tweet Left Join `retweet` as retweet on retweet.TweetsID = tweet.TweetsID  Left JOIN `user` as users on users.UserID = retweet.UserID WHERE retweet.UserID = 27 OR tweet.UserID = 27
        //$stmt = $conn->prepare("SELECT c1.TweetsID as tweetsID, c1.UserID as ID,c1.Content as Tweets ,c2.Firstname ,c2.Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate From `tweets` as c1 LEFT JOIN `user` as c2 on users.UserID = retweet.UserID Left Join `retweet` as retweet on retweet.TweetsID = tweet.TweetsID  WHERE retweet.UserID = 27 OR c1.UserID = 27");
    //Previous//$stmt = $conn->prepare("SELECT c1.TweetsID as tweetsID, c1.UserID as ID,c1.Content as Tweets ,c2.Firstname as Firstname ,c2.Lastname as Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate, retweet.RetweetID as retweetkey From `tweets` as c1 Left Join `retweet` as retweet on retweet.TweetsID = c1.TweetsID Left JOIN `user` as c2 on c2.UserID = retweet.UserID WHERE retweet.UserID = ? OR c1.UserID = ? ");

    $stmt = $conn->prepare("SELECT c1.TweetsID as tweetsID, c1.UserID as ID,c1.Content as Tweets ,c2.Email as Email,c2.Firstname as Firstname ,c2.Lastname as Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate, retweet.RetweetID as retweetkey,retweet.UserID as retweetUserID From `tweets` as c1 Left JOIN `retweet` as retweet on retweet.TweetsID = c1.TweetsID Left JOIN `user` as c2 on c2.UserID = c1.UserID WHERE retweet.UserID = ? OR c1.UserID = ?");

    $stmt->bind_param("ss", $identification, $identification);
    echo '<div id ="div1-wrapper">';
    echo '<div id="div1"> ';
    echo 'here are the tweets specifically by the user <br>';

    $hearts = checkHearts($conn, $identification);

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        echo '<form method="post">';
        if ($result->num_rows == 0) {
            echo '<br> You have not twitted yet <br>';
        }
        $heartsCounter = 0;
        $i = 0;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                if (!empty($row["retweetkey"]))
                    if ($row["retweetUserID"] == $_GET["name"]) {
                    echo "this is a retweet :D ";
                }
                $Email = $row["Email"];
                echo '<script src="/MicroAjax/jquery-1.12.3.min.js" type="text/javascript"></script>';
                chdir("../microview");
                $filename = ('uploads/' . $Email . '.jpg');
                if (file_exists($filename)) {
                    $name = $Email;
                } else
                    $name = "default";
                echo '<img src="uploads/' . $name . '.jpg" style = "height: 100px ;width: 100px ;float:left ;">';
                echo "Firstname " . $row["Firstname"] . "<br>";
                echo "Lastname " . $Lastname = $row["Lastname"] . "<br>";
                echo "dates " . $dates = $row["Date"] . "<br>";

                echo "Tweets " . $tweets = $row["Tweets"] . "<br>";
    
/* SELECT tweet.TweetsID,follow.UserID from `tweets` as tweet Right JOIN `following` as follow on follow.UserID = tweet.UserID LEFT Join `user` as users on follow.UserID  = users.UserID WHERE follow.FollowersID = 39 
UNION
SELECT c1.TweetsID as tweetsID,c2.UserID From `tweets` as c1 Left JOIN `retweet` as retweet on retweet.TweetsID = c1.TweetsID Left JOIN `user` as c2 on c2.UserID = c1.UserID WHERE retweet.UserID = 39 OR c1.UserID = 39*/
                //  echo '<input type="submit" name = "button[' . $i . ']" value = "comment">';
                if (isset($_COOKIE['user'])) {
                    if ($_COOKIE['user'] === $_GET['name']) {
                    //  echo '<input type="submit" name = "button[' . $i . ']" value = "comment">';
                        echo '<input type="submit" class="w3-btn w3-orange"name = "button[' . $i . ']" value = "retweet">';
                        echo '<input type="submit" class="w3-btn w3-orange" name = "button[' . $i . ']" value = "delete">';
                        echo '<input type="submit" class="w3-btn w3-orange" name = "button[' . $i . ']" value = "edit">';
                    } else {
                        echo '<input type="submit" class="w3-btn w3-orange" name = "button[' . $i . ']" value = "retweet">';
                    }
                    if ($hearts != null) {
                        $flag = true;
                        for ($j = 0; $j < count($hearts); $j++) {
                            if ($flag == true) {
                                if ($row["tweetsID"] == $hearts[$j][0]) {
                                    echo '<input type="submit"class="w3-btn w3-orange" name = "button[' . $i . ']" value = "unheart"><hr>';
                                    $flag = false;
                                }
                            }
                        }
                        if ($flag == true) {
                            echo '<input type="submit"class="w3-btn w3-orange" name = "button[' . $i . ']" value = "heart"><hr>';
                            $flag = true;

                        } else {
                            $flag = true;
                        }
                    }
                }

                echo '<hr>';
                $i++;
                $my_array[] = array($tweets = $row["Tweets"], $Firstname = $row["Firstname"], $Lastname = $row["Lastname"], $DateCreated = $row["Date"], $retweetid = $row["retweetkey"]);
            }
            return $my_array;
        }

        echo '</form>';
    }
    echo '</div>';
    echo '</div>';
    $stmt = $conn->prepare("SELECT c1.UserID as ID,c1.Content as Tweets ,c2.Firstname ,c2.Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate  From `tweets` as c1 LEFT JOIN `user` as c2 on c1.UserID = c2.UserID where c2.UserID = ?");

}
?>