<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Microblog Edit Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/MicroAjax/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script src="/MicroAjax/main.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/Template.css" />
</head>
<body>
<div id="mySidenav" class="sidenav">
<?php 
if (!isset($_COOKIE['user'])) {
  echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
  echo '<a href="#">About</a>';
  echo '<a href="login.php">Login</a>';
  echo '<a href="register.php">Register</a>';
  echo '<a href="#">Contact</a>';
} else {
  echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
  echo '<a href="profile.php?name=' . $_COOKIE['user'] . '">Profile</a>';
  echo '<a href="profileedit.php">Settings</a>';
  echo '<a href="logout.php">Logout</a>';
}
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");



?>
</div>
<div class="w3-blue-background w3-right w3-container"></div>

<div class="w3-black w3-bar w3-large">
<?php 
if (isset($_COOKIE['user']))
  echo ' <a href="loggedinHome2.php?name=' . $_COOKIE['user'] . '"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
else
  echo ' <a href="login.php?"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
?>
  <a href="#" class="w3-bar-item w3-button w3-mobile w3-margin-left">About</a>
  <input type="text" class="w3-bar-item w3-input w3-white w3-mobile" placeholder="Search.." id="Searchbar" style="margin-left: 250px">
  <div id="livesearch"></div> 
     <button class="w3-bar-item w3-button w3-black w3-mobile"id = "Go">Go</button>
  <span style="font-size:30px;cursor:pointer" class ="w3-right w3-margin-right" onclick="openNav()">&#9776; Menu</span>
</div>

<div id="main">
<center>
<div class="w3-container">
<form method="POST" enctype="multipart/form-data">
    <div class="containerregister" style="background-color: #515B51">
    <img src="uploads/default.jpg" height="200" alt="Image preview..." id="testimage">
    <input type="file" onchange="previewFile();" id="imageFile" name="imageFile" value="file"><br>
    <hr>
    First Name: <input type="text" name="FirstName" id="FirstName" require><div id="Firstname"><br>
    Last Name: <input type="text" name="LastName" id="LastName" require><div id="Lastname"><br>
    <hr>
    Password: <input type="password" name="registerPassword" id="registerPassword" require><div id="Password"><br>
    Confirm Password <input type="password" name="registerCpassword" id="registerCpassword" require><br>
    <hr>
    <input type="button" value = "Submit" name="button" id="editsubmit"  onclick="this.form.submit();" >
    
    
    </div>
  </form>

</div>
</center>
</div>
<footer class="w"
<footer class="w3-black" style="bottom: 0px; position: relative; width: 100%">
	<center>
  <p>Posted by: Albert Rey Ruelan</p></center>

<?php

chdir('../Micromodel');
include 'db.php';
$db = "microblog";
$conn = dbConnection($db);
$identification = $_COOKIE['user'];
$target_dir = "";
$imageFile = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //sleep(5);
  if (!file_exists('uploads/')) {
    mkdir('uploads/', 0777, true);
  }
  $stmt = $conn->prepare("SELECT * FROM user where UserID = ?");
  $stmt->bind_param("s", $identification);
  if ($stmt->execute()) {
    $result = $stmt->get_result();
    if ($result->num_rows == 0) {
      return false;
    }
    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $Email = $row["Email"];
      }

    }
  }
  $target_dir = "uploads/";
  $target_file = $target_dir . $Email . "." . "jpg";
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

  // Check if image file is a actual image or fake image


  $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
  if ($check !== false) {
    echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    echo "File is not an image.";
    $uploadOk = 0;
  }


  if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
  } else {
    chdir('../Microview');

    move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file);
    $identification = $_COOKIE['user'];


  }


}

?>



</body><script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
</html>