<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Microblog Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/MicroAjax/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script src="/MicroAjax/main.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/Template.css" />
</head>
<body>
<div id="mySidenav" class="sidenav">
<?php 
if (!isset($_COOKIE['user'])) {
    echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
    echo '<a href="#">About</a>';
    echo '<a href="login.php">Login</a>';
    echo '<a href="register.php">Register</a>';
    echo '<a href="#">Contact</a>';
} else {
    echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
    echo '<a href="profile.php?name=' . $_COOKIE['user'] . '">Profile</a>';
    echo '<a href="profileedit.php">Settings</a>';
    echo '<a href="logout.php">Logout</a>';
}


?>
</div>
<div class="w3-blue-background w3-right w3-container"></div>

<div class="w3-black w3-bar w3-large">
<?php 
if (isset($_COOKIE['user']))
    echo ' <a href="loggedinHome2.php?name=' . $_COOKIE['user'] . '"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
else
    echo ' <a href="login.php?"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
?>
  <a href="#" class="w3-bar-item w3-button w3-mobile w3-margin-left">About</a>
  <input type="text" class="w3-bar-item w3-input w3-white w3-mobile" placeholder="Search.." id="Searchbar" style="margin-left: 250px">
  <div id="livesearch"></div> 
     <button class="w3-bar-item w3-button w3-black w3-mobile"id = "Go">Go</button>
  <span style="font-size:30px;cursor:pointer" class ="w3-right w3-margin-right" onclick="openNav()">&#9776; Menu</span>
</div>
        <div id="main">
<center>
<div class="w3-container" div class="container" style="background-color: #515B51">
    <?php 
    chdir("../Microcontroller");
    include 'db.php';
    $db = "microblog";
    $conn = dbConnection($db);
    $confirmation = $_GET['code'];
                //$stmt = $conn->prepare("Select Username,Password from user where Username = ? AND Password = ?");
    $stmt = $conn->prepare("SELECT * FROM temp_user WHERE `Activation Code` = ?");
    $stmt->bind_param("s", $confirmation);
    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                if ($row['Activation Code'] === $confirmation && $row['Purpose'] === "Email") {
                    echo 'Congratulations you have now activated your account';
                    $stmt2 = $conn->prepare("DELETE FROM temp_user where `Activation Code` = ?");
                    $stmt2->bind_param("s", $confirmation);
                    if ($stmt2->execute()) {
                        echo '<br>';
                        echo 'You may now login your Account <br>';
                        echo 'You will now be automatically sent to the login page in 5 seconds';
                        sleep(10);
                        header("refresh:5;url=login.php");
                    }

                } elseif ($row['Activation Code'] === $confirmation && $row['Purpose'] === "forgotpassword") {
                    echo '<br> Your UserID is: <input type="text" name="newpass" id="UserID" value="' . $row['UserID'] . '" disabled>';
                    echo '<br> New Password: <input type="password" name="newpass" id="NewforgotPass">';
                    echo '<br> Confirm Passsword: <input type="password" name="newpass" id="ConfirmforgotPass">';
                    echo '<input type="submit" class="btn" value="Submit" id="changePassword">';

                }


            }
        } else {
            echo 'There seems to be something wrong with your confirmation email.';
            echo '<br>';
            echo 'It could either <br> 1. Your had already confirmed your email <br>2. You had already changed your password <br>3. The database could not find your code in the database';
        }
    }

    ?>
    
</div>
</center>
</div>
<footer class="w"
<footer class="w3-black" style="bottom: 0px; position: relative; width: 100%">
	<center>
  <p>Posted by: Albert Rey Ruelan</p></center>

    <?php 
    ?>



</body><script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
</html>