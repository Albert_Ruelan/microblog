<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../css/Template.css" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Microblog Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/MicroAjax/jquery-1.12.3.min.js" type="text/javascript"></script>
    <script src="/MicroAjax/main.js"></script>

</head>
<body>
<div id="mySidenav" class="sidenav">
<?php 
if (!isset($_COOKIE['user'])) {
    echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
    echo '<a href="#">About</a>';
    echo '<a href="login.php">Login</a>';
    echo '<a href="register.php">Register</a>';
    echo '<a href="#">Contact</a>';
} else {
    echo '<a href="javascript:void(0)"  onclick="closeNav()"> Close&times;</a>';
    echo '<a href="profile.php?name=' . $_COOKIE['user'] . '">Profile</a>';
    echo '<a href="profileedit.php">Settings</a>';
    echo '<a href="logout.php">Logout</a>';
}


?>
</div>
<div class="w3-blue-background w3-right w3-container"></div>

<div class="w3-black w3-bar w3-large">
<?php 
if (isset($_COOKIE['user']))
    echo ' <a href="loggedinHome2.php?name=' . $_COOKIE['user'] . '"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
else
    echo ' <a href="login.php?"   class="w3-bar-item w3-button w3-mobile w3-margin-left w3-large">Home</a>';
?>
  <a href="#" class="w3-bar-item w3-button w3-mobile w3-margin-left">About</a>
  <input type="text" class="w3-bar-item w3-input w3-white w3-mobile" placeholder="Search.." id="Searchbar" style="margin-left: 250px">
  <div id="livesearch"></div> 
     <button class="w3-bar-item w3-button w3-black w3-mobile"id = "Go">Go</button>
  <span style="font-size:30px;cursor:pointer" class ="w3-right w3-margin-right" onclick="openNav()">&#9776; Menu</span>
</div>

<div id="main">
<div class="w3-container">
<form method="post">
    <div class="container" style="background-color: #515B51">


     Email:  <input type="text" name="loginName" id="loginName" required><br>
     Password: <input type="password" name="password" id="password" required><br>
     
     <input type="button" class="btn"value="Login"  id="login"></button>
     <input type="button" class="btn"value="Register" id="register" style="margin-top: 10px"></button>
    
     </div>
  </form>

</div>
    </div>
    <footer class="w3-black" style="bottom: 0px; position: fixed; width: 100%">
	<center>
  <p>Posted by: Albert Rey Ruelan</p></center>
</footer>
<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

</body>


</html>