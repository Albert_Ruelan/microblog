<?php 
include 'UserDAO.php';
include 'db.php';


class UserDAOIMPL implements UserDAO
{

      private $vars = array();
      private $userID;
      public function getPictures($id){
            //insert photo pa
            $user_array = $this->getAlluserByID($id);
            //chdir("./htdocs/microview/uploads");
            $filename = ('./microview/uploads/'.$user_array[2].'.jpg');
           if(file_exists($filename)){
                  return $user_array[2];
            }
            else 
                  return "default";
            
      }
      public function getAlluserByID($id){
            $conn=$this->getConnection();
            $stmt= $conn->prepare("SELECT * FROM user where UserID = ?");
            $stmt->bind_param("s",$id);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        return false;
                  }
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              $my_array = array($Firstname = $row["Firstname"], $Lastname = $row["Lastname"], $Email = $row["Email"], $DateCreated = $row["DateCreated"]);
                        }
                        return $my_array;
                  }
            }    


      }
      public function getRegister($registerFirstName, $registerLastName, $registerPassword, $registerEmail)
      {
            $registerFirstName = $this->test_input($registerFirstName);
            $registerLastName = $this->test_input($registerLastName);
            $registerPassword = $this->test_input($registerPassword);
            $registerEmail = $this->test_input($registerEmail);

            $conn = $this->getConnection();
            $mysqltimeCurrent = date("Y-m-d H:i:s");
            $confirm_code = md5(uniqid(rand()));
            $registerPassword=hash('sha512',$registerPassword);
            //$stmt = $conn->prepare("INSERT INTO user ()");
            //$stmt = $conn->prepare("INSERT INTO `user` ( `Firstname`,`Lastname `, `Password`,`Email`,`DateCreated`,`DateUpdated`   ) VALUES ( ? , ? , ? , ? , ? , ? )");
            $stmt = $conn->prepare("INSERT INTO `user` ( `Firstname`, `Lastname`, `Password`, `Email`, `DateCreated`, `DateUpdated`) VALUES ( ?, ?, ? , ?,?, ?)");
            $stmt->bind_param("ssssss", $registerFirstName, $registerLastName, $registerPassword, $registerEmail, $mysqltimeCurrent, $mysqltimeCurrent);
            if ($stmt->execute()) {
                  echo 'Registration Successful';
            } else {
                  echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
            $Confirmation = "Email";
            $last_id = $conn->insert_id;
            $this->getPhoto($last_id,$registerEmail);
            $this->temp_user($confirm_code, $last_id, $mysqltimeCurrent, $Confirmation);
            $this->sendConfirmation($confirm_code, $registerEmail);
            exit();
      }
      
      function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
      }
      public function getPhoto($last_id,$email){
            $conn = $this->getConnection();
            $stmt2 = $conn->prepare("INSERT INTO `photo` ( `UserID`, `PhotoLocation`, `PhotoName`) VALUES ( ?, ?, ?)");
            $location = "/microview/uploads";
            $photoname = $email;
            $stmt2->bind_param("sss",$last_id,$location,$photoname);
            $stmt2->execute();
      }
      public function getEditUser($editFirstName,$editLastName,$editPassword ){
            $id = $_COOKIE['user'];
            $user_array = $this->getAlluserByID($id);
            $conn = $this->getConnection();
            $Firstname = $user_array[0];
            $Lastname = $user_array[1];            
            $password=$user_array[2];

            $editFirstName = $this->test_input($editFirstName);
            $editLastName = $this->test_input($editLastName);
            $editPassword = $this->test_input($editPassword);

            if($Firstname !== $editFirstName && $editFirstName !== ""){
                  $Firstname= $editFirstName  ;
            }
            if($Lastname !== $editLastName && $editLastName !== ""){
                  $Lastname = $editLastName;
            }
            if($password !== $editPassword &&  $editPassword !== ""){
                  $password = $editPassword;
            }
            $mysqltimeCurrent = date("Y-m-d H:i:s");
            $password=hash('sha512',$password);

            $stmt = $conn->prepare("UPDATE `user` SET Firstname=?,Lastname=?,Password = ?,DateUpdated=? Where UserID= ?");
            $stmt->bind_param("sssss", $Firstname, $Lastname,$password,$mysqltimeCurrent,$id);
            if ($status = $stmt->execute()) {
                  echo 'User Profile updated succesfully';
                  if ($status === false) {
                        trigger_error($stmt->error, E_USER_ERROR);
                  }
            }
      }


      public function getUserEmail($email){
            $email = $this->test_input($email);

            $conn=$this->getConnection();
            $stmt= $conn->prepare("SELECT * FROM user where Email = ?");
            $stmt->bind_param("s",$email);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        return false;
                  }
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              $my_array = array($Firstname = $row["Firstname"], $Lastname = $row["Lastname"], $Email = $row["Email"], $DateCreated = $row["DateCreated"],$id = $row["UserID"]);
                        }
                        return $my_array;
                  }
            }     
      }
      public function forgotPassword($Email){
            $Email = $this->test_input($Email);

           $checker= $this->getUserEmail($Email);
           $confirm_code = md5(uniqid(rand()));
           $mysqltimeCurrent = date("Y-m-d H:i:s");
            $confirmation = "forgotpassword";
            if($checker!==false){
                  $userID=$checker[4];
                  $this->temp_user($confirm_code,$userID,$mysqltimeCurrent,$confirmation);
                  $this->sendConfirmation($confirm_code, $Email);
            }else{
                  echo 'No registered email in the database. Please Try again';
            }
      }
      public function temp_user($confirm_code, $last_id, $mysqltimeCurrent, $Confirmation)
      {
            $conn = $this->getConnection();
            $stmt = $conn->prepare("INSERT INTO `temp_user` (`Activation Code`, `UserID`, `DateCreated`, `Purpose`) VALUES (?,?,?,?)");
            $stmt->bind_param("ssss", $confirm_code, $last_id, $mysqltimeCurrent, $Confirmation);
            if ($stmt->execute()) {
                  echo 'Success';
            } else {
                  echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
      }
      public function deletetemp_user($userID){
            $conn = $this->getConnection();
            $stmt = $conn->prepare("DELETE FROM `temp_user` where UserID = ?");
            $stmt->bind_param("s", $userID);//27 28
            $stmt->execute();
      }
      public function searchtemp_user($email){
            $conn = $this->getConnection();
            $stmt = $conn->prepare("SELECT * FROM `temp_user` as temp LEFT JOIN `user` as users on temp.UserID = users.UserID where users.Email = ?");
            $stmt->bind_param("s",$email);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        return false;
                  }
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              $my_array = array($Firstname = $row["Purpose"],$Email = $row["Email"]);
                        }
                        return $my_array;
                  }
            }     

      }
      public function sendConfirmation($confirm_code, $registerEmail)
      {
            require 'mailer/Exception.php';
            require 'mailer/PHPMailer.php';
            require 'mailer/SMTP.php';

            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            try {
                //Server settings
                  $mail->isSMTP();                                      // Set mailer to use SMTP
                  $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                  $mail->SMTPAuth = true;                               // Enable SMTP authentication
                  $mail->Username = 'albert.ruelan07@gmail.com';                 // SMTP username
                  $mail->Password = 'albertrey';                           // SMTP password
                  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                  $mail->Port = 587;                                    // TCP port to connect to

                  $mail->setFrom('albert.ruelan07@gmail.com', 'Mailer');
                  $mail->addAddress($registerEmail, 'Albert Ruelan');     // Add a recipient
                  $mail->addReplyTo('albert.ruelan07@gmail.com', 'Information');
                  $mail->addCC('cc@example.com');
                  $mail->addBCC('bcc@example.com');
            
                //Content
                  $mail->isHTML(true);                                  // Set email format to HTML
                  $mail->Subject = 'Confirmation Email';
                  $mail->Body = "This is your confirmation link : ";
                  $mail->Body .= "http://localhost/Microview/activation.php?code=$confirm_code";

                  $mail->send();
                  echo 'Message has been sent';
            } catch (Exception $e) {
                  echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }
      }
      public function getTweet($identification)
      {
            $conn = $this->getConnection();
            //Note: isa isahin na lang for tweet and get tweet
            $stmt = $conn->prepare("SELECT TweetsID as tweetsid,UserID as user,Content as content,DateCreated as dates  from tweets where User");
            $stmt->bind_param("ss", $identification, $identification);

            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        echo '<br> You have not twitted yet <br>';
                  }
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              $my_array[] = array($tweets = $row["content"], $id = $row["user"], $DateCreated = $row["dates"], $tweetsId = $row["tweetsID"]);
                        }
                        return $my_array;
                  }
            }
      }
      public function paginationTweets($limit,$pageCount,$linkCount){
            $identificaiton = $_COOKIE['user'];
            $conn = $this->getConnection();
            $limit1 = intval($limit-5);
            $limit2 = intval($limit);

            $stmt = $conn->prepare("Select * from `tweets` as tweet LEFT JOIN `following` as follow on follow.UserID = tweet.UserID LEFT Join `user` as users on tweet.UserID = users.UserID WHERE follow.FollowersID = ? OR tweet.UserID = ? LIMIT ?,? ");
            $stmt->bind_param("ssii", $identificaiton,$identificaiton,$limit1,$limit2);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        echo '<br> You have not twitted yet <br>';
                  }
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                             //original $my_array[] = array($tweets = $row["Content"], $id = $row["UserID"], $DateCreated = $row["DateCreated"], $tweetsId = $row["TweetsID"],$Firstname = $row["Firstname"],$Lastname = $row["Lastname"]);
                              $my_array[]= array('Content' => $tweets = $row['Content'], 'UserID' => $suggestLname = $row['UserID'],'DateCreated' => $suggestID = $row['DateCreated'], 'TweetsID' => $suggestLname = $row['TweetsID'],'Firstname' => $suggestID = $row['Firstname'],'Lastname' => $suggestID = $row['Lastname']);

                        }
                        return $my_array;
                  }
            }
      }
      public function follow($id)
      {
            $identification=$_COOKIE['user'];
            $conn = $this->getConnection();
            $mysqltimeCurrent = date("Y-m-d H:i:s");
            
            $checker = $this->checkiffollowing($identification,$id);
            if($checker ===false){
                  return false;
            }
            else{
            $stmt = $conn->prepare("INSERT INTO `following` (`UserID`,`FollowersID`,`DateFollowed`) VALUES(?,?,?) ");
            $stmt->bind_param("sss", $id, $identification, $mysqltimeCurrent);
            $stmt->execute();
            return true;}
      }
      public function checkiffollowing($followersID,$userID){
            $conn = $this->getConnection();
            $stmt = $conn->prepare("Select * from `following` WHERE UserID = ? AND FollowersID = ?");
            $stmt->bind_param("ss",$userID,$followersID);

            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        return true;
                  }
                  else{
                        return false;
                  }
            }


      }
      public function allTweets($identificaiton)
      {
            //getting all the following users tweets
            $conn = $this->getConnection();
            $stmt = $conn->prepare("Select * from `tweets` as tweet LEFT JOIN `following` as follow on follow.UserID = tweet.UserID WHERE follow.FollowersID = ? OR tweet.UserID = ? LIMIT ?,?");

            $stmt->bind_param("s", $identificaiton);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 0) {
                        echo '<br> You have not twitted yet <br>';
                  }
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              $my_array[] = array($tweets = $row["content"], $id = $row["user"], $DateCreated = $row["dates"], $tweetsId = $row["tweetsID"]);
                        
                        }
                        return $my_array;
                  }
            }
      }
      public function getReTweet($identification)
      {
            // try to get it one by one
            $conn = $this->getConnection();

            $stmt = $conn->prepare("SELECT c1.TweetsID as tweetsID, c1.UserID as ID,c1.Content as Tweets ,c2.Firstname ,c2.Lastname, c1.DateCreated as Date, c2.DateCreated as UserDate, retweet.RetweetID as retweetkey From `tweets` as c1 Left Join `retweet` as retweet on retweet.TweetsID = c1.TweetsID Left JOIN `user` as c2 on c2.UserID = retweet.UserID WHERE retweet.UserID = ");
            $stmt->bind_param("ss", $identification, $identification);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              $my_array[] = array($tweets = $row["Tweets"], $Firstname = $row["Firstname"], $Lastname = $row["Lastname"], $DateCreated = $row["Date"], $tweetsId = $row["tweetsID"], $retweetid = $row["retweetkey"]);
                        }
                        return $my_array;
                  }
            }

      }

      public function getUser($loginname, $loginpass)
      {
            $loginname = $this->test_input($loginname);
            $loginpass = $this->test_input($loginpass);

            $conn = $this->getConnection();
            $loginEmail = $loginname;
            if (isset($_COOKIE['user'])) {
                  unset($_COOKIE['user']);

            }
            $password=hash('sha512',$loginpass);
            $stmt = $conn->prepare("Select UserID,Email,Password from user where Email = ? AND Password = ?");
            $stmt->bind_param("ss", $loginEmail, $password);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                              if ($row["Email"] == $loginEmail && $row["Password"] == $password) {
                                    $cookie_name = "user";
                                    $cookie_value = $row["UserID"];
                                    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
                                    $userID = $row["UserID"];
                                    return $row["UserID"];
                                    exit;

                              }
                        }
                  }
            }
            return false;
      }

      public function resetPassword($newpass, $userID)
      {
            $newpass = $this->test_input($newpass);

            $conn = $this->getConnection();
            $newpass=hash('sha512',$newpass);
            $stmt = $conn->prepare("UPDATE user SET Password = ? Where UserID= ?");
            $stmt->bind_param("ss", $newpass, $userID);
            if ($status = $stmt->execute()) {

                  echo 'Password updated succesfully';
                  $this->deletetemp_user($userID);
                  if ($status === false) {
                        trigger_error($stmt->error, E_USER_ERROR);
                  }
            }
            return false;

      }
      public function getQuery($obj)
      {
            $conn = $this->getConnection();
            $stmt = $conn->prepare("SELECT Firstname,Lastname,UserID,Email FROM User WHERE Firstname LIKE CONCAT('%',?,'%')");
            $stmt->bind_param("s", $obj);
            if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {

                              $searchArray[]= array('Firstname' => $suggestFname = $row['Firstname'], 'Secondname' => $suggestLname = $row['Lastname'],'ID' => $suggestID = $row['UserID'],'Email' => $email = $row['Email']);

                        }
                        return json_encode($searchArray,true);
                  }
            }

      }

      public function getHearts($identification)
      {
            $conn = getConnection();

            $stmt = $conn->prepare("INSERT INTO `hearts` (`TweetsID`,`UserID`,`DateCreated`) VALUES(?,?,?)");
            $stmt->bind_param("sss", $id, $identification, $mysqltimeCurrent);
            $stmt->execute();
      }

      public function getConnection()
      {
    //include_once('../Micromodel/LoginModel.php');
            $db = "microblog";
            $conn = dbConnection($db);
            return $conn;
      }
      public function editTweet($TweetID, $TweetContent,$TweetDate){
            $TweetContent = $this->test_input($TweetContent);
            
            $conn = $this->getConnection();
            $mysqltimeCurrent = date("Y-m-d H:i:s");

            $stmt = $conn->prepare("UPDATE tweets SET Content =?,DateCreated=? Where TweetsID=?");
            $stmt->bind_param("sss",$TweetContent,$mysqltimeCurrent,$TweetID);
            if ($status = $stmt->execute()) {
                  echo 'Tweet updated succesfully';
                  
                  if ($status === false) {
                        trigger_error($stmt->error, E_USER_ERROR);
                  }
            }
      }

      


}

?>