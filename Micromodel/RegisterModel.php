<?php 

 class Register{
    public $Username;
    public $Password;
    public $Email;
    public $DateCreated;
    public $DateUpdated;

    public function __construct($Username,$Password,$Email,$DateCreated,$DateUpdated)
    {
        $this->Username = $Username;
        $this->Password = $Password;
        $this->Email = $Email;
        $this->DateCreated = $DateCreated;
        $this->DateUpdated = $DateUpdated;
    }
 }

?>