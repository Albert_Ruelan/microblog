$(document).ready(function () {
    $("#register").click(function () {
        window.location = "/Microview/register.php";
    });
    $("#Go").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var str = document.getElementById('Searchbar').value;
        if (str.length == 0) {
            document.getElementById("livesearch").innerHTML = "";
            document.getElementById("livesearch").style.border = "0px";
            return;
        }
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // var myArr = JSON.parse(this.responseText);
                var return_data = xmlhttp.responseText;
                window.location = "/Microview/searchperson.php?query=" + xmlhttp.responseText;

            }
        }
        xmlhttp.open("POST", "/indexMicroBlog.php", true);

        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        // use for json xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.send("query=" + str);
    });
    $("#editsubmit").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var FirstName = document.getElementById('FirstName').value;
        var LastName = document.getElementById('LastName').value;
        var registerPassword = document.getElementById('registerPassword').value;
        var registerCpassword = document.getElementById('registerCpassword').value;
        if (LastName === "" && FirstName == "" && Email == "") {
            string = string + "No user edit has been made \n";
            alert(string);

        } else {
            var edit = new Register(FirstName, LastName, registerPassword, registerCpassword, Email);
            if (registerPassword == registerCpassword) {
                edit = JSON.stringify(edit);

                xmlhttp.open("GET", "/indexMicroBlog.php?editprofile=" + edit, true);
                xmlhttp.send();

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        var return_data = xmlhttp.responseText;
                        alert(return_data);
                        //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
                    }
                };
            }
        }
    })
    $("#ProfileFollow").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var id = document.getElementById('UserID').value;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var return_data = xmlhttp.responseText;
                if (return_data == true) {
                    alert('You are now following')
                }
                else
                    alert('you are already following him');
                //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
            }

            //window.location="/Microview/profile.php?name=" + xmlhttp.responseText;
        };
        xmlhttp.open("GET", "/indexMicroBlog.php?followidbyprofile=" + id, true);
        xmlhttp.send();

    });

    $("#registersubmit").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var FirstName = document.getElementById('FirstName').value;
        var LastName = document.getElementById('LastName').value;
        var Email = document.getElementById('Email').value;
        var registerPassword = document.getElementById('registerPassword').value;
        var registerCpassword = document.getElementById('registerCpassword').value;
        var count = 3;
        var string = "";
        if (LastName === "") {
            string = string + "Please enter lastname \n";
            count = count - 1;
        }
        if (FirstName == "") {
            string = string + "Please enter Firstname \n";
            count = count - 1;

        }
        if (Email === "") {
            string = string + "Please enter Email \n";
            count = count - 1;
        }
        var register = new Register(FirstName, LastName, registerPassword, registerCpassword, Email);
        if (count > 2) {

            if (registerPassword == registerCpassword) {
                register = JSON.stringify(register);
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        var return_data = xmlhttp.responseText;
                        alert(return_data);
                        //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
                    }
                };
                xmlhttp.open("GET", "/indexMicroBlog.php?register=" + register, true);
                xmlhttp.send();
            }
        }
        else {
            alert(string);
        }
    });
    //forgot password
    $("#forgotpassword").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var email = document.getElementById('Email').value;
        var email = new Email(email);
        email = JSON.stringify(email);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var return_data = xmlhttp.responseText;
                alert(return_data);
                //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
            }

            //window.location="/Microview/profile.php?name=" + xmlhttp.responseText;
        };
        xmlhttp.open("GET", "/indexMicroBlog.php?forgotpassword=" + email, true);
        xmlhttp.send();


    });

    ///change password
    $("#changePassword").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var UserID = document.getElementById('UserID').value;
        var firstpass = document.getElementById('NewforgotPass').value;
        var secondpass = document.getElementById('ConfirmforgotPass').value;
        var password = new Password(UserID, firstpass, secondpass);
        if (firstpass == secondpass) {
            password = JSON.stringify(password);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var return_data = xmlhttp.responseText;
                    if (return_data == "Password updated succesfully")
                        alert(" Password Updated Successfully \n You will now be redirected to the Home page");
                    window.location = "/Microview/login.php";

                }

                //window.location="/Microview/profile.php?name=" + xmlhttp.responseText;
            };
            xmlhttp.open("GET", "/indexMicroBlog.php?password=" + password, true);
            xmlhttp.send();
        }

    });
    //////login

    $("#login").click(function () {
        var xmlhttp = new XMLHttpRequest();
        name = document.getElementById('loginName').value;
        password = document.getElementById('password').value;
        //var hello = document.getElementById('loginName')
        var login = new User(name, password);
        login = JSON.stringify(login);

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var return_data = xmlhttp.responseText;
                if (return_data.localeCompare('Unsuccessful login') == 0) {
                    alert(return_data);
                }
                else if (return_data.localeCompare('Email') == 0) {
                    alert('Verify your email first');
                }
                else {
                    window.location = "/Microview/loggedinHome2.php?name=" + xmlhttp.responseText;
                }


            }
        };
        xmlhttp.open("GET", "/indexMicroBlog.php?login=" + login, true);
        xmlhttp.send();

    });
    ///////// FollowMe
    $("#followMe").click(function () {
        var xmlhttp = new XMLHttpRequest();
        var id = document.getElementById("UserID").value;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                var return_data = xmlhttp.responseText;
                if (return_data == true) {
                    alert("You are now following");
                }
                else {
                    alert("You are already following him")
                }
            }
        }
        xmlhttp.open("GET", "/indexMicroBlog.php?followidbyprofile=" + id, true);
        xmlhttp.send();

    });
    ///////// Second Parameter
    $("#register").click(function () {

        var xmlhttp = new XMLHttpRequest();
        if (document.getElementById('registerPassword').value == document.getElementById('registerCpassword').value) {

            register = new Register(document.getElementById('FirstName').value, document.getElementById('LastName').value, document.getElementById('registerPassword').value, document.getElementById('registerCpassword').value);
            register = JSON.stringify(register);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var return_data = xmlhttp.responseText;
                    alert(return_data + "hello");
                    //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
                }
            };
            xmlhttp.open("GET", "/Microcontroller/registerLogic.php?register=" + register, true);
            xmlhttp.send();
        }
        else {
            alert('Password does not match');
        }

    });
    $("#editweets").click(function () {

        var xmlhttp = new XMLHttpRequest();
        TweetId = document.getElementById('tweetid').value;
        TweetContent = document.getElementById('tweet').value;
        Tweetdate = document.getElementById('tweetdate').value;

        tweets = new Tweets(TweetId, TweetContent, Tweetdate)
        tweets = JSON.stringify(tweets);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var return_data = xmlhttp.responseText;
                alert("hello" + return_data);
                //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "/indexMicroBlog.php?editweets=" + tweets, true);
        xmlhttp.send();


    });

});
function loadData(limitCount, pageCount, linkCount) {
    var xmlhttp = new XMLHttpRequest();


    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

            var myArr = JSON.parse(this.responseText);

            thediv = document.getElementById("name");
            thecontent = document.getElementById("content");

            for (var i = 0; i < myArr.length; i++)
                var obj = myArr[i];
            //       thediv.innerHTML =  thediv.innerHTML +"Username : "+ myArr[i].Firstname + myArr[i].Lastname + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"+"Date Created: " +myArr[i].DateCreated+'<br>'
            //     + myArr[i].Content +'<hr>'+'<br>';

            console.log("Item name: " + name);
            console.log("Source: " + obj.UserID);
            console.log("Content: " + obj.Content);
            console.log("Content: " + obj.DateCreated);
            console.log("Content: " + obj.Firstname);
            console.log("Content: " + obj.Lastname);
            console.log("Content: " + obj.TweetsID);
            console.log("Content: " + obj.UserID);
        }
    };
    xmlhttp.open("GET", "/indexMicroBlog.php?limit=" + limitCount + "&pageCount=" + pageCount + "&linkCount=" + linkCount, true);
    xmlhttp.send();
}

function validateUsername(Firstname, Lastname) {
    var re = /^[A-Za-z]+$/;
    if (re.test(Firstname) && re.test(Lastname))
        return true;
    else
        return false;

}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(email).toLowerCase()))
        return true;
    else
        return false;
}

function pictures(id) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var return_data = xmlhttp.responseText;

            var img = new Image();
            var div = document.getElementById('testimage');

            img.onload = function () {
                div.appendChild(img);
            };
            img.style = 'height: 300px ;width: 300px';
            img.src = 'uploads/' + return_data + '.jpg';

        }
    };
    var set = 'saysomething';
    xmlhttp.open("POST", "/indexMicroBlog.php?picture=" + id, true);

    xmlhttp.send();
}
function pictures2(id) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var return_data = xmlhttp.responseText;

            var img = new Image();
            var div = document.getElementById('testimage2');

            img.onload = function () {
                div.appendChild(img);
            };
            img.style = 'height: 300px ;width: 300px';
            img.src = 'uploads/' + return_data + '.jpg';

        }
    };
    var set = 'saysomething';
    xmlhttp.open("POST", "/indexMicroBlog.php?picture=" + id, true);

    xmlhttp.send();
}
function editTweet(Tweetid) {
    window.location = "/Microview/editTweet.php?tweetID=" + Tweetid;
}


//////////////////////Variables Section


function previewFile() {
    var preview = document.querySelector('img');
    // var filelocation = document.querySelector('imageFile').src
    //var files=document.getElementById("imageFile").src;
    var file = document.querySelector('input[type=file]').files[0];

    var imgFile = document.getElementById('imageFile');

    var reader = new FileReader();
    var xmlhttp = new XMLHttpRequest();



    reader.onloadend = function () {
        preview.src = reader.result;

    }

    if (file) {

        reader.readAsDataURL(file); //reads the data as a URL



    } else {
        preview.src = "";
    }
}

function Tweets(TweetId, TweetContent, Tweetdate) {
    this.TweetId = TweetId;
    this.TweetContent = TweetContent;
    this.Tweetdate = Tweetdate;
}

function imagetoBase(Image) {
    this.Image = Image;
}

function Email(Email) {
    this.Email = Email;
}

function User(loginName, password) {
    this.loginName = loginName;
    this.password = password;
}
function Password(UserID, newpass, oldpass) {
    this.UserID = UserID;
    this.newpass = newpass;
    this.oldpass = oldpass;
}

function Register(registerFirstName, registerLastName, registerPassword, registerCpassword, registerEmail) {
    this.registerFirstName = registerFirstName;
    this.registerLastName = registerLastName;
    this.registerPassword = registerPassword;
    this.registerCpassword = registerCpassword;
    this.registerEmail = registerEmail;

}

$(function () {
    $("loginName").click(function () {
        var xmlhttp = new XMLHttpRequest();
        name = document.getElementById('loginName').value
        password = document.getElementById('password').value
        //var hello = document.getElementById('loginName')
        var login = new User(name, password);


    });


});

/*
function afterConfirmation(){
    var xmlhttp = new XMLHttpRequest();
    
}
function retweet(){
    var xmlhttp = new XMLHttpRequest();
}
fucntion gettweet(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "/Microcontroller/registerLogic.php?register=" + register, true);

}

function register(){
    var xmlhttp = new XMLHttpRequest();
    if(document.getElementById('registerPassword').value == document.getElementById('registerCpassword').value){
        var register = {
           // "registerPhoto": document.getElementById('fileToUpload').files[0].name,
            "registerFirstName": document.getElementById('FirstName').value,
            "registerLastName": document.getElementById('LastName').value,
            "registerPassword": document.getElementById('registerPassword').value,
            "registerCpassword": document.getElementById('registerCpassword').value,
            "registerEmail": document.getElementById('Email').value,
        }
        register = JSON.stringify(register);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var return_data = xmlhttp.responseText;
                alert(return_data + "hello");
             //   window.location="/Microview/profile.php?=" + xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "/Microcontroller/registerLogic.php?register=" + register, true);
        xmlhttp.send();
    }
    else{
        alert('Password does not match');
    }
}*/



